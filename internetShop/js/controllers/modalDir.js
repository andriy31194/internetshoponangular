app.directive('modalDialog', ['UserService',function(UserService){
  return {
    restrict: 'E',
    scope: {
      show: '='
    },
    replace: true, // Replace with the template below
    transclude: true, // we want to insert custom content inside the directive
    link: function(scope, element, attrs) {
      scope.dialogStyle = {};
      if (attrs.width)
        scope.dialogStyle.width = attrs.width;
      if (attrs.height)
        scope.dialogStyle.height = attrs.height;
      scope.hideModal = function() {
        scope.show = false;
      };
         scope.register=function(){
      var obj = {
          name:scope.name,
          sname:scope.sname,
          login:scope.login,
          password:scope.password
      }
      // function to submit the form after all validation has occurred			
		scope.submitForm = function() {

			// check to make sure the form is completely valid
			if (scope.userForm.$valid) {
				alert('submit');
			}

		};
//      console.log(obj);
              UserService.registration(obj);
             scope.hideModal();
      }
    },
    templateUrl:'/templates/modal.html'
  };
}]);

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var app = express();

app.use(bodyParser.json());
app.use(cors());

app.get('/', function(request, response){
    response.send('Hello i am api');
});

var buyers = [];
var sugges=[];
var users=[
    {
        id: 354,
        name: 'Andriy',
        sname: 'Krutyi',
        login: '1',
        password: '1'
    },
          {
        id: 235,
        name: 'admin',
        sname: 'admin',
        login: 'admin',
        password: 'admin'
    }
          ];
var products = [
    {
        id:1,
        name:'Компютер',
        price:10,
        about:'опис товару',
        idCateg:11,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:2,
        name:'Компютер',
        price:15,
        about:'опис товару',
        idCateg:11,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:3,
        name:'Компютер',
        price:13,
        about:'опис товару',
        idCateg:22,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:4,
        name:'Компютер',
        price:17,
        about:'опис товару',
        idCateg:33,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:5,
        name:'Компютер',
        price:24,
        about:'опис товару',
        idCateg:22, photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:6,
        name:'Компютер',
        price:10,
        about:'опис товару',
        idCateg:11,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:7,
        name:'Компютер',
        price:15,
        about:'опис товару',
        idCateg:11,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:8,
        name:'Компютер',
        price:13,
        about:'опис товару',
        idCateg:22,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:9,
        name:'Компютер',
        price:17,
        about:'опис товару',
        idCateg:33,
        photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:10,
        name:'Компютер',
        price:24,
        about:'опис товару',
        idCateg:22, photo:'https://s.dns-shop.ru/up/blog/cache/full/12743.1445401222.jpg',
        countProd:1
    },
    {
        id:11,
        name:'ноутбук',
        price:85,
        about:'опис товару',
        idCateg:44,
        photo:'https://st03.kakprosto.ru//images/article/2014/6/25/130132_53aaffb9dd96153aaffb9dd998.jpeg',
        countProd:1
    },
     {
        id:12,
        name:'ноутбук',
        price:94,
        about:'опис товару',
        idCateg:44,
        photo:'https://st03.kakprosto.ru//images/article/2014/6/25/130132_53aaffb9dd96153aaffb9dd998.jpeg',
        countProd:1
    },
    {
        id:13,
        name:'ноутбук',
        price:90,
        about:'опис товару',
        idCateg:44,
        photo:'https://st03.kakprosto.ru//images/article/2014/6/25/130132_53aaffb9dd96153aaffb9dd998.jpeg',
        countProd:1
    },
    {
        id:14,
        name:'ноутбук',
        price:87,
        about:'опис товару',
        idCateg:44,
        photo:'https://st03.kakprosto.ru//images/article/2014/6/25/130132_53aaffb9dd96153aaffb9dd998.jpeg',
        countProd:1
    },
    {
        id:15,
        name:'процесор',
        price:15,
        about:'опис товару',
        idCateg:55,
        photo:'http://www.microbs.ru/hardware_pc/img/case2.jpg',
        countProd:1
    },
    {
        id:16,
        name:'процесор',
        price:15,
        about:'опис товару',
        idCateg:55,
        photo:'http://www.microbs.ru/hardware_pc/img/case2.jpg',
        countProd:1
    },
    {
        id:17,
        name:'процесор',
        price:15,
        about:'опис товару',
        idCateg:55,
        photo:'http://www.microbs.ru/hardware_pc/img/case2.jpg',
        countProd:1
    }
];
var category = [
    {
        id:11,
        name:'Samsung'
    },
    {
        id:22,
        name:'Sony'
    },
    {
        id:33,
        name:'Apple'
    },
    {
        id:44,
        name:'Lenovo'
    },
    {
        id:55,
        name:'Акція!!!'
    }
];

app.get('/products', function(req, res){
    res.send(products);
});
app.get('/category', function(req, res){
    res.send(category);
});

app.get('/users', function(req, res){
    res.send(users);
});
app.get('/sugges', function(req, res){
    res.send(sugges);
});
app.post('/suggest', function(req, res){
    var sugg = req.body;
    sugges.push(sugg);
    var obj = Object.assign({},sugg);
    res.send(obj);
           
});

app.post('/login', function(req, res){
    var user = req.body;
    for(var i in users){
        if(users[i].login==user.login && users[i].password==user.password){
            var obj = Object.assign({},users[i]);
            delete obj.password;
            res.send(obj);
            return;
        }
    }
    res.send(false);
    return;
    
});

app.post('/registr', function(req, res){
    var user = req.body;
    user.id = Date.now();
    users.push(user);
    var obj = Object.assign({},user);
     delete obj.password;
    res.send(obj);
           
});


app.post('/addProduct',function(req, res){
   var item = req.body;
    item.id = products[products.length-1].id+1;
    products.push(item);
    res.send(item);
});

app.post('/addCategory',function(req, res){
   var item = req.body;
//    item.id = category[category.length-1].id+1;
    category.push(item);
    res.send(item);
});

app.post('/changeProd',function(req,res){
    var item = req.body;
    
    for(var i  in products){
       if(item.id===products[i].id){
        products.splice(i,1,item)
        }
    }
    res.send(item);
})
app.post('/changeCat',function(req,res){
    var item = req.body;
    
    for(var i  in category){
       if(item.id===category[i].id){
        category.splice(i,1,item)
        }
    }
    res.send(item);
})

app.delete('/removeProduct/:id',function(req,res){
    var id = req.params.id;
    for(var i in products){
        if(products[i].id==id){
            products.splice(i,1);
            res.send(true);
            return;
        }
    }
    res.send(false);
    
})

app.delete('/removeCategory/:id',function(req,res){
    var id = req.params.id;
    for(var i in category){
        if(category[i].id==id){
            category.splice(i,1);
            res.send(true);
            return;
        }
    }
    res.send(false);
    
})



app.listen(8081, function(){
    console.log('listen on port')
});